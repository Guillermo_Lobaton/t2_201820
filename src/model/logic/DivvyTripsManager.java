package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import api.IDivvyTripsManager;
import model.vo.VOByke;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Nodo;

public class DivvyTripsManager implements IDivvyTripsManager {
	private DoublyLinkedList<Nodo> viajes;
	private DoublyLinkedList<Nodo> estaciones;

	public void loadStations (String stationsFile) {


		String csvFile = "data/Divvy_Stations_2017_Q3Q4.csv";
		BufferedReader br = null;
		String linea = "";
	    
		
		try {
			br = new BufferedReader(new FileReader(csvFile));
			try {
				
				
				
				while ((linea = br.readLine()) != null) {                
					String[] datos = linea.split(",");
					List<String> arreglo = Arrays.asList(datos);
					Nodo<List<String> > actual = new Nodo<List<String>  >(arreglo );
					estaciones.agregarElemento(actual);
					
					
					
					
				}
			} catch (IOException e) {
		
				e.printStackTrace();
			}
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		

			}

	

			public void loadTrips (String tripsFile) {
				
				
				String csvFile = "data/Divvy_Trips_2017_Q4.csv";
				BufferedReader br = null;
				String linea = "";
			    
				
				try {
					br = new BufferedReader(new FileReader(csvFile));
					try {
						
						
						
						while ((linea = br.readLine()) != null) {                
							String[] datos = linea.split(",");
							List<String> arreglo = Arrays.asList(datos);
							Nodo<List<String> > actual = new Nodo<List<String>  >(arreglo );
							estaciones.agregarElemento(actual);
							
							
							
						}
					} catch (IOException e) {
				
						e.printStackTrace();
					}
				}
				catch (FileNotFoundException e) {
					e.printStackTrace();
				} 
				

					}

			

			@Override
			public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
				// TODO Auto-generated method stub
				if(viajes.getSize() == 0) {
					loadTrips ("");
				}
				
				DoublyLinkedList <VOTrip> listaF = new DoublyLinkedList<VOTrip>();
					Iterator<Nodo> it = viajes.iterator();
					
					
					while(it.hasNext()) {
					Nodo actual = it.next();
					String[] arregloEnNodo = (String[]) actual.darElemento();
					if(gender.equals(arregloEnNodo[9])){
						listaF.agregarElemento(actual);
					}
					
					
				}
					return listaF;
				
			}

			@Override
			public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
				// TODO Auto-generated method stub
				if(viajes.getSize() == 0) {
					loadTrips ("");
				}
				
				DoublyLinkedList <VOTrip> listaF = new DoublyLinkedList<VOTrip>();
					Iterator<Nodo> it = viajes.iterator();
					
					
					while(it.hasNext()) {
					Nodo actual = it.next();
					String[] arregloEnNodo = (String[]) actual.darElemento();
					if((Integer.toString(stationID)).equals(arregloEnNodo[0])){
						listaF.agregarElemento(actual);
					}
					
					
				}
					return listaF;
				
			}
			


			@Override
			public void loadServices(String stationsFile) {
				// TODO Auto-generated method stub

			}


		}
