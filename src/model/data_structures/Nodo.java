package model.data_structures;

public class Nodo<T> {

	private T elemento;
	private Nodo<T> siguiente;
	private Nodo<T> anterior;
	
	public Nodo (T elem) {
		elemento = elem;
		anterior= null;
		siguiente = null;
	}
	public T darElemento() {
		return elemento;
	}
	
	public Nodo<T> darAnterior() {
		return anterior;
	}
	public Nodo<T> darSiguiente() {
		return siguiente;
	}
	public void cambiarAnterior(Nodo<T> e) {
		anterior = e;
	}
	public void cambiarSiguiente(Nodo<T> e) {
		siguiente = e;
	}
}
