package model.data_structures;

import java.util.Iterator;
import java.util.LinkedList;


public class DoublyLinkedList <T> implements IDoublyLinkedList<T> {
    
	//Atributos
	private Nodo<T> primero;
	
	
	//Constructor
	public DoublyLinkedList()
	{
		primero = null;
		
	}
	
	//Metodos
	public Nodo<T> darCabeza(){
		return primero;
	}
	
	
	
	@Override
	public int getSize() {
		int contador = 0;
		if(primero !=null) {
		Nodo<T> actual = primero;
		while(actual.darSiguiente()!= null) {
		 contador++;	
		 actual = actual.darSiguiente();
		}
				}
		
		return  contador;
		
	}

	@Override
	public Iterator<T> iterator() {
		return null;
		// TODO Auto-generated method stub
	   
	}
	
	public void agregarElemento(Nodo elemento) {
		
		if(primero == null) {
			primero = (Nodo) elemento;
			
		}
		else {
			Nodo  actual = primero;
			while(actual.darSiguiente()!= null){
				if(actual.darSiguiente()== null) {
					actual.cambiarSiguiente(elemento);
					elemento.cambiarAnterior(actual);
					elemento.cambiarSiguiente(null);
				}
			}
		}
	}

}
